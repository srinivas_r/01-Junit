package com.demo.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CalculatorTest {
	
	static Calculator calc;
	
	@BeforeAll
	public static void setUpBeforeAll() {
		System.out.println("setUpBeforeAll() called");
		calc = new Calculator();
	
	}
	
	@AfterAll
	public static void tearDownAfterAll() {
		System.out.println("tearDownAfterAll() called");
		calc = null;
	}
	
	
	/**
	 * @Test indicates that it is a test method
	 * The method must not be private and must
	 * not be static. The return type must be void
	 */
	@Test
	public void additionTest() {
		
		int actualResult = calc.addition(2, 3);
		int expectedResult = 5;
		assertEquals(expectedResult, actualResult);
		
	}
	
	@Test
	public void multiplyTest() {
		
		assertEquals(8, calc.multiply(2,4));
		
	}

}
